package tree_sitter_Verdigris_test

import (
	"testing"

	tree_sitter "github.com/smacker/go-tree-sitter"
	"github.com/tree-sitter/tree-sitter-Verdigris"
)

func TestCanLoadGrammar(t *testing.T) {
	language := tree_sitter.NewLanguage(tree_sitter_Verdigris.Language())
	if language == nil {
		t.Errorf("Error loading Verdigris grammar")
	}
}
